import { Fragment, useState, useEffect } from "react";

import Formulario from "./components/Formulario";
import Cita from "./components/Cita";

function App() {

  // obtener citas si hay en localstorage
  const previas = JSON.parse(localStorage.getItem('citas')) || [];

  // Crear sate principal que contiene las cits
  const [citas, actualizarCitas] = useState(previas);

  // Detecta cuando el state citas cambia

  useEffect( () => {
    const previas = JSON.parse(localStorage.getItem('citas')) || [];
    if (previas) {
      localStorage.setItem('citas', JSON.stringify(citas));
    } else {
      localStorage.setItem('citas', JSON.stringify([]));
    }
  }, [citas]);

  // funcion que agrege la cita
  const crearCita = (cita) => {
    // Agregamos una copia del state y la nueva cita
    actualizarCitas([...citas, cita]);
  }

  // Elimin una cita por su id
  const eliminarCita = _id => {
    actualizarCitas( citas.filter(cita => cita._id !== _id) );
  }

  // Mensaje condicional
  const titulo = citas.length ? 'Citas' : 'No hay citas';

  return (
    <Fragment>
      <h1>Administrador de citas Veterinaria</h1>
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Formulario
              crearCita={crearCita}
            />
          </div>
          <div className="one-half column">
            <h2>{ titulo }</h2>
            {
              citas.map( cita => (
                <Cita
                  key={cita._id}
                  cita={cita}
                  eliminarCita={eliminarCita}
                />
              ))
            }
          </div>
        </div>
      </div>
    </Fragment>
  );
}


export default App;
