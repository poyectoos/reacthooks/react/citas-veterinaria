import PropTypes from "prop-types";

const Cita = ({ cita, eliminarCita }) => {
  
  const { _id, mascota, propietario, fecha, hora, sintoma } = cita;
  return (
    <div className="cita">
      <p><b>Mascota: </b><span>{ mascota }</span></p>
      <p><b>Propietario: </b><span>{ propietario }</span></p>
      <p><b>Fecha: </b><span>{ fecha }</span></p>
      <p><b>Hora: </b><span>{ hora }</span></p>
      <p><b>Sintoma: </b><span>{ sintoma }</span></p>
      <div className="row">
        <div className="one-half column">
          <button
            className="button eliminar u-full-width"
            onClick={() => eliminarCita(_id)}
          >
            Eliminar
          </button>
        </div>
      </div>
    </div>
  );
};

Cita.propTypes = {
  cita: PropTypes.object.isRequired,
  eliminarCita: PropTypes.func.isRequired
}

export default Cita;