// <reference types="" />

import { Fragment, useState } from 'react';
import { v4 } from "uuid";
import PropTypes from "prop-types";

const Formulario = ({ crearCita }) => {

  // Crear State de citas
  const [cita, actualizarCita] = useState({
    mascota: '',
    propietario: '',
    fecha: '',
    hora: '',
    sintoma: ''
  });
  // Crear state para mostrar error
  const [error, actualizarError] = useState(false);

  // Funcion que se ejecuta cuando un input cambia
  const actualizarState = (e) => {
    actualizarCita({ ...cita, [e.target.name]: e.target.value });
  }

  // Estraer los valores
  const { mascota, propietario, fecha, hora, sintoma } = cita;

  // Funcion que se ejecuta cuando se envia el formulario
  const submitCita = (e) => {
    e.preventDefault();
    // Validar
    if (
      mascota.trim() === '' ||
      propietario.trim() === '' ||
      fecha.trim() === '' ||
      hora.trim() === '' ||
      sintoma.trim() === ''
    ) {
      actualizarError(true);
      return;
    }
    actualizarError(false);
    
    // Asignamos el id
    const _id = v4();

    // Agregamos las citas al state principal
    crearCita({ _id, ...cita });
    
    // Reiniciamos formulario
    actualizarCita({
      mascota: '',
      propietario: '',
      fecha: '',
      hora: '',
      sintoma: ''
    });
    
  }


  return (
    <Fragment>
      <h2>Crear cita</h2>
      { 
        error
          ?
        <p className="alerta-error">Todos los camapos son obligatorios</p>
          :
        null
      }
      <form onSubmit={submitCita}>
        <label htmlFor="mascota">Masctora</label>
        <input
          type="text"
          name="mascota"
          id="mascota"
          className="u-full-width"
          onInput={actualizarState}
          value={mascota}
        />
        <label htmlFor="propietario">Dueño</label>
        <input
          type="text"
          name="propietario"
          id="propietario"
          className="u-full-width"
          onInput={actualizarState}
          value={propietario}
        />
        <label htmlFor="fecha">Fecha</label>
        <input
          type="date"
          name="fecha"
          id="fecha"
          className="u-full-width"
          onChange={actualizarState}
          value={fecha}
        />
        <label htmlFor="hora">Hora</label>
        <input
          type="time"
          name="hora"
          id="hora"
          className="u-full-width"
          onChange={actualizarState}
          value={hora}
        />
        <label htmlFor="sintoma">Sintomas</label>
        <textarea
          name="sintoma"
          id="sintoma"
          rows="3"
          className="u-full-width"
          onInput={actualizarState}
          value={sintoma}
        ></textarea>

        <button
          type="submit"
          className="u-full-width button-primary"
        >Crear</button>
      </form>
    </Fragment>
  );
};

Formulario.propTypes = {
  crearCita: PropTypes.func.isRequired
}
export default Formulario;